import React from 'react';
import ReactDOM from 'react-dom';
import "./style/main.less";

class Welcome extends React.Component {
    render () {
        return <h1>Hello World from React boilerplate 123</h1>;
    }
}

// eslint-disable-next-line no-undef
ReactDOM.render(<Welcome />, document.getElementById('app'));
