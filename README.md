## Features

* React 16
* Webpack 4
* Babel 7
* Hot Module Replacement


## Alternatives

* [Advanced React Webpack Babel Setup](https://github.com/rwieruch/advanced-react-webpack-babel-setup)

## Installation

* `git clone https://ditavn@bitbucket.org/ditavn/react-webpack-babel-setup.git`
* cd minimal-react-webpack-babel-setup
* npm install
* npm start
* visit `http://localhost:8080/`
